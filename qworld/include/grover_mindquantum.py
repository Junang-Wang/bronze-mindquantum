from mindquantum import *

def Uf(circuit):
    circuit += X.on(2,[0,1])

def Uf8(circuit):
    circuit += X.on(2)
    circuit += X.on(4,[2,1])
    circuit += X.on(3,[4,0])
    circuit += X.on(4,[2,1])
    circuit += X.on(2)